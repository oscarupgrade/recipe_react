import React from 'react'

import style from './recipes.module.scss';

 const Recipes = ({title, calories, image, ingredients}) => {
    return (
        <div className={style.recipe}>
            <h1>{title}</h1>
            <ol>
                {ingredients.map(ingredient =>(
                    <li>{ingredient.text}</li>
                ))}
            </ol>
            <p> Calorias: {calories} kcal</p>
            <img className={style.img} src={image} alt='imagen receta' />
        </div>
    )
}

export default Recipes;
