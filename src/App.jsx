import React, {useEffect, useState} from 'react';
import Recipes from './Recipes.jsx';

import './App.scss';

function App() {

  const APP_ID = "b1bf941c";
  const APP_KEY = "12a7af29af4e19e7adccdd1578934b31";

  const [recipes, setRecipes] = useState([]);
  const [search, setSearch] = useState('');
  const [query, setQuery] = useState("chicken")

  useEffect(() => {
    const getRecipes = async () =>{
      const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`);
      const data = await response.json();
      setRecipes(data.hits)
    };
    getRecipes();
  }, [query]);

  

  const updateSearch = (e) => {
    setSearch(e.target.value);
  };

  const getSearch = (e) => {
    e.preventDefault();
    setQuery(search);
    setSearch('');
  }

  return (
    <div className="App">
      <form onSubmit={getSearch} className='search__form'>
        <input className='search__form__bar' type='text' value={search} onChange={updateSearch} />
        <button className='search__form__button' type='submit'>Search</button>
      </form>
      <div className='recipes'>
      {recipes.map(recipe =>(
        <Recipes
        key={recipe.recipe.label} 
        title={recipe.recipe.label}
        calories={recipe.recipe.calories}
        image={recipe.recipe.image}
        ingredients={recipe.recipe.ingredients} />
      ) )}
      </div>
    </div>
  );
}

export default App;
